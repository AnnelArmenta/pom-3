const BaseElement = require("./common/cloudPage.component.js");

const SearchBox = require("./main/searchBox.component.js");
const SearchResults = require("./main/resultsLookup.component.js");

const CreateComputeEstimate = require("./calculatorMain/createComputeEstimate.component.js");
const NewComputeEstimateValue = require("./calculatorMain/newComputeEstimateSetValue.component.js");
const NewComputeEstimateField = require("./calculatorMain/newComputeEstimateField.component.js");
const ShadowGPU = require("./shadowSelectors/shadowGPU.component.js");
const EstimateValue= require("./calculatorMain/estimateValue.component.js");
const MainPageYopmail= require("./yopmail/mainpage.component.js");
const EmailGenerated= require("./yopmail/emailGenerated.component.js");
const InboxEmail= require("./yopmail/inboxEmail.component.js");
const SendEmail= require("./calculatorMain/sendEmail.component.js");
const iFramesGoogle= require("./calculatorMain/iframes.component.js");


module.exports = {
    BaseElement,
    SearchBox,
    SearchResults,
    CreateComputeEstimate,
    NewComputeEstimateValue,
    NewComputeEstimateField,
    ShadowGPU,
    EstimateValue,
    MainPageYopmail,
    EmailGenerated,
    InboxEmail,
    SendEmail,
    iFramesGoogle,
}