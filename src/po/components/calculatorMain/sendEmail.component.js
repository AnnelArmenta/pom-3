const baseComponent=require("./../common/cloudPage.component")
class sendEmail extends baseComponent{
    constructor(){
        super('form[name=emailForm]');
    }
    get estimateEmail(){
        return  $('button[id="Email Estimate"]');
    }
    get sendEmail(){
        return this.rootEl.$('button.md-raised');
    }
    get inputEmail(){
        return $('input[type="email"]');
    }
}

module.exports=sendEmail;