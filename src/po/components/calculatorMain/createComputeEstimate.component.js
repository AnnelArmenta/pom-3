const baseComponent = require("../common/cloudPage.component.js");

class CreateComputeEstimate extends baseComponent{

    constructor(){
        super();
    }

    get createEstimateButton(){
        return $('span[jsname="V67aGc"]');
    }

    get computeEngineOption(){
        return $('div[jscontroller="KRZHBd"]');
    }

}

module.exports = CreateComputeEstimate;