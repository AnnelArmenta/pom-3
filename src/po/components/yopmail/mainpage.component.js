class yopmailMainPage{
    get emailGenerator(){
        return $('(//a[@href="email-generator"])[2]');
    }
    get iframe1(){
        return $('#aswift_6');
    }
    get iframe2(){
        return $('#ad_iframe');
    }
    get dismissButton(){
        return $('#dismiss-button');
    }
}

module.exports=yopmailMainPage;