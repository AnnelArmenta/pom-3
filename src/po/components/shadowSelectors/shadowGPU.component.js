const baseComponent = require("../common/cloudPage.component.js");

class ShadowComponents extends baseComponent{

    constructor(){
        super();
    }

    get shadowGPUModel(){
        return $('(//div[@jscontroller="GPHYJd"])[8]').shadow$('div[aria-labelledby="c12599 c12601"]');
    }

    get shadowNumberOfGPUs(){
        return $('(//div[@jscontroller="GPHYJd"])[9]').shadow$('div[aria-labelledby="c6615 c6617"]');
    }

}

module.exports = ShadowComponents;