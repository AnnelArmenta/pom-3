class baseHeader{

    constructor(rootSelector){
        this.rootSelector = rootSelector;
    }

    get rootEl(){
        return $(this.rootSelector);
    }

}

module.exports = baseHeader;