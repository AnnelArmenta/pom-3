const baseHeader = require("../common/cloudPage.component.js");


class searchBox extends baseHeader{

    constructor(){
        super('div.a7K4Uc');
    }

    get searchButton(){
        return $('div[jsname="Ohx1pb"]');
    }

    get searchInput(){
        const input = $('input[class="qdOxv-fmcmS-wGMbrd"]').shadow$('div[jsname="btrqz"]');
        return input;
    }

    get searchEnter(){
        return this.rootEl.$(' i[jsname="Z5wyCf"]');
    }

}

module.exports = searchBox;