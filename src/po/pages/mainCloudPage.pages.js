const { SearchBox, SearchResults } = require("../components/index.js");
const pageOpener = require("./pageOpener.pages.js");

class MainCloudPage extends pageOpener{

    constructor(){
        super('https://cloud.google.com/');
        this.searchBox = new SearchBox();
        this.searchResults = new SearchResults();
    }
}

module.exports = MainCloudPage;