const { EmailGenerated, MainPageYopmail, InboxEmail} = require("../components/index.js");
const pageOpener = require("./pageOpener.pages.js");


class YopMailPage extends pageOpener{

    constructor(){
        super('https://yopmail.com/');
        this.EmailGenerated = new EmailGenerated();
        this.MainPageYopmail = new MainPageYopmail();
        this.InboxEmail = new InboxEmail();

    }
}

module.exports = YopMailPage;