class basePage{

        constructor(url){

            this.url = url;

        }

        open(){
            return browser.url(this.url);
        }
}

module.exports = basePage;