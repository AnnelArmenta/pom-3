const YopMailPage = require("./yopMailPage.pages.js");
const MainCloudPage = require("./mainCloudPage.pages.js");
const CalculatorPage = require("./calculatorPage.pages.js");

/*
* @params page {'mainCloudPage' | 'yopMailPage' | 'calculator'}
* @returns {'MainCloudPage' | 'YopMailPage' | 'CalculatorPage'}
*/

function pages(page){
    const possiblePages = {
        mainCloudPage: new MainCloudPage(),
        yopMailPage: new YopMailPage(),
        calculator: new CalculatorPage(),
    }
    return possiblePages[page];
}

module.exports = {
    MainCloudPage,
    YopMailPage,
    CalculatorPage,
    pages,
}