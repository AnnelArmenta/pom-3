const {pages} = require("./../po");
describe("Cloud Calculator", ()=>{

    beforeEach(async ()=>{
        await pages('mainCloudPage').open();
        await browser.maximizeWindow();
    });

    it("Create Google cloud estimate and send to customer", async()=>{

        await browser.pause(3000);

        await pages('mainCloudPage').searchBox.searchButton.click();
        await browser.execute((text) => {
            document.activeElement.value = text;
          }, 'Google Cloud Platform Pricing Calculator');
        await browser.keys("\uE007");
        await pages('mainCloudPage').searchResults.item('calculatorLegacy').click();
        await browser.pause(500);
        
        await browser.switchToFrame(await pages('calculator').iFramesGoogle.iframeG1);
        await browser.switchToFrame(await pages('calculator').iFramesGoogle.iframeG2);

        await pages('calculator').newComputeEstimateField.input('numberOfInstances').setValue('4');
        await browser.pause(2000);
        await pages('calculator').newComputeEstimateField.dropdown('operatingSystem').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.dropdown('operatingSystem').click();
        await pages('calculator').newComputeEstimateValue.operatingSystem('free_Debian_Ubuntu_BYOL').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.operatingSystem('free_Debian_Ubuntu_BYOL').click();
        
        await pages('calculator').newComputeEstimateField.dropdown('provisioningModel').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.dropdown('provisioningModel').click();
        await pages('calculator').newComputeEstimateValue.provisioningModel('regular').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.provisioningModel('regular').click();

        await pages('calculator').newComputeEstimateField.dropdown('machineFamily').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.dropdown('machineFamily').click();
        await pages('calculator').newComputeEstimateValue.machineFamily('General_Purpose').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.machineFamily('General_Purpose').click();

        await pages('calculator').newComputeEstimateField.dropdown('series').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.dropdown('series').click();
        await pages('calculator').newComputeEstimateValue.series('N1').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.series('N1').click();

        await pages('calculator').newComputeEstimateField.dropdown('machineType').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.dropdown('machineType').click();
        await pages('calculator').newComputeEstimateValue.machineType('n1_standard_8').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.machineType('n1_standard_8').click();

        await pages('calculator').newComputeEstimateField.addGPUs.scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.addGPUs.click();
      
        await pages('calculator').newComputeEstimateField.dropdown('GPUModel').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.dropdown('GPUModel').click();
        await pages('calculator').newComputeEstimateValue.GPUModel('NVIDIA_Tesla_V100').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.GPUModel('NVIDIA_Tesla_V100').click();
        
        await pages('calculator').newComputeEstimateField.dropdown('numberOfGPUs').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.dropdown('numberOfGPUs').click();
        await pages('calculator').newComputeEstimateValue.numberOfGPUs('li_1').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.numberOfGPUs('li_1').click();
        
        await pages('calculator').newComputeEstimateField.dropdown('localSSD').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.dropdown('localSSD').click();
        await pages('calculator').newComputeEstimateValue.localSSD('li_2x375GB').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.localSSD('li_2x375GB').click();
        await pages('calculator').newComputeEstimateField.dropdown('region').scrollIntoView({block: 'center', inline: 'center'});
        
        await pages('calculator').newComputeEstimateField.dropdown('region').click();
        await pages('calculator').newComputeEstimateValue.region('Netherlands').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateValue.region('Netherlands').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.region('Netherlands').click();
        await pages('calculator').newComputeEstimateField.dropdown('committedUsage').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.dropdown('committedUsage').click();
        await pages('calculator').newComputeEstimateValue.committedUsage('oneYear').waitForDisplayed({timeout:6000});
        await pages('calculator').newComputeEstimateValue.committedUsage('oneYear').click();
        await pages('calculator').newComputeEstimateField.addEstimateButton.scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').newComputeEstimateField.addEstimateButton.click();
        await pages('calculator').estimateValue.resultBlock.waitForDisplayed();
        await browser.pause(2000);
        const valuegoogle= await pages('calculator').estimateValue.valueGoogle.getText();
        // clean valuegoogle
        const numericValue = valuegoogle.match(/[\d,\.]+/)[0];
        const cleanNumericValue = numericValue.replace(/,/g, '');
        
        browser.newWindow('https://yopmail.com/')
        await pages('yopMailPage').MainPageYopmail.emailGenerator.waitForDisplayed({timeout:60000})
        await pages('yopMailPage').MainPageYopmail.emailGenerator.click();
        await browser.pause(6000);
        // access iframes

        const adframe1 = await pages('yopMailPage').MainPageYopmail.iframe1;
        await browser.switchToFrame(adframe1);
        const dismissButton = await pages('yopMailPage').MainPageYopmail.dismissButton;
        if (await dismissButton.isDisplayed()) {
            await dismissButton.waitForDisplayed();
            await dismissButton.click();
        } else {
            await browser.switchToParentFrame();
            await browser.switchToFrame(adframe1);
            const adframe2 = await pages('yopMailPage').MainPageYopmail.iframe2;
            await browser.switchToFrame(adframe2);
            await dismissButton.waitForDisplayed();
            await dismissButton.click();
        }
       

        await browser.switchToParentFrame();
        await browser.switchToParentFrame();

        await pages('yopMailPage').EmailGenerated.copyEmail.scrollIntoView({block: 'center', inline: 'center'});
        await pages('yopMailPage').EmailGenerated.copyEmail.waitForClickable();
        await pages('yopMailPage').EmailGenerated.copyEmail.click();

        await browser.switchWindow('//cloud.google.com/')
        await browser.switchToFrame(await pages('calculator').iFramesGoogle.iframeG1);
        await browser.switchToFrame(await pages('calculator').iFramesGoogle.iframeG2);
       
        await pages('calculator').sendEmail.estimateEmail.scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').sendEmail.estimateEmail.click();

        await pages('calculator').sendEmail.inputEmail.waitForClickable();
        await pages('calculator').sendEmail.inputEmail.click();
        await browser.keys(['Control','v']);
        await browser.pause(2000);

        await pages('calculator').sendEmail.sendEmail.scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculator').sendEmail.sendEmail.waitForClickable();
        await pages('calculator').sendEmail.sendEmail.click();

        await browser.switchWindow('https://yopmail.com/')
        await browser.pause(5000);
        await browser.switchToParentFrame();
        await browser.switchToParentFrame();

        await pages('yopMailPage').EmailGenerated.openInbox.waitForClickable();
        await pages('yopMailPage').EmailGenerated.openInbox.click();

        const reCAPTCHAFrame=await pages('yopMailPage').InboxEmail.reCAPTCHA;
        if (await reCAPTCHAFrame.isExisting()) {
            console.log('Hay un reCAPTCHA al abrir el correo, por tal motivo, no se puede continuar');
        } else {
            await browser.switchToFrame(await pages('yopMailPage').InboxEmail.iframEmail);
            const value1 = await pages('yopMailPage').InboxEmail.valueIframe.getText();
            const numericValue1 = value1.match(/[\d,\.]+/)[0];
            const cleanNumericValue1 = numericValue1.replace(/,/g, '');

            await browser.pause(2000);
            await expect(cleanNumericValue).toEqual(cleanNumericValue1);
        }


         await browser.pause(1000);

    });
})

